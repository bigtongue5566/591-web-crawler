//引用 nodemailer
const nodemailer = require('./node_modules/nodemailer/lib/nodemailer')
const config = require('./config') 
//宣告發信物件
module.exports = function(rentArr,type){

    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.gmailAccount,
            pass: config.gmailPassword
        }
    })
    let message = ''
    for(let rentObj of rentArr){
        message += `<h3 style="margin-top:10px">${rentObj.address_img}<span style="margin-left:10px">${rentObj.price}</span><h3><a href="https://rent.591.com.tw/rent-detail-${rentObj.id}.html">https://rent.591.com.tw/rent-detail-${rentObj.id}.html</a>`
    }
    const options = {
        //寄件者
        from: config.gmailAccount,
        //收件者
        to: config.recipients, 
        //主旨
        subject: `591新增物件-${type}`, // Subject line
        //嵌入 html 的內文
        html: message, 
    };

    //發送信件方法
    transporter.sendMail(options, function(error, info){
        if(error){
            console.log(error);
        }else{
            console.log('訊息發送: ' + info.response);
        }
    });
}
