const electron = require('electron')
const notifier = require('node-notifier')
const Store = require('electron-store');
const store = new Store();
const {ipcMain} = require('electron')
const config = require('./config')
const mail = require('./mail')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const rentUrl = 'https://rent.591.com.tw'
const intervalSeconds = config.intervalSeconds*1000 
const startTime = 5*1000
let mainWindow ,nextWindow,fourRoomStore,threeRoomStore

function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 600})
  mainWindow.on('closed', function () {
    mainWindow = null
  })
  mainWindow.loadURL(rentUrl)
  let targetArray = []
  targetArray.push({type:'中永和3房',url:'https://rent.591.com.tw/home/search/rsList?is_new_list=1&type=1&kind=0&searchtype=1&region=3&section=37,38&pattern=3&order=posttime&orderType=desc&rentpriceMore=4,3'})
  //targetArray.push({type:'中永和4房',url:'https://rent.591.com.tw/home/search/rsList?is_new_list=1&type=1&kind=0&searchtype=1&region=3&pattern=4&rentprice=,32000&section=38,37&order=posttime&orderType=desc'})
  targetArray.push({type:'中正大安文山3房',url:'https://rent.591.com.tw/home/search/rsList?is_new_list=1&type=1&kind=0&searchtype=1&region=1&patternMore=3&section=5,1,12&rentprice=,25000&order=posttime&orderType=desc'})
  runScript(targetArray)
  //setInterval(()=>{runScript(targetArray)},intervalSeconds)
}

function runScript(targetArray){
  let tempTime = startTime
  let script = `let ipcRenderer = require('electron').ipcRenderer\n` 
  for(let target of targetArray){
    let region = getUrlParameter(target.url,'region')
    script += `setTimeout(()=>{          
      $('[data-ie="search-location-span"][data-index="1"]').click()
    },${tempTime})\n`
    tempTime+=3000
    script += `setTimeout(()=>{     
      $('[data-type="region"][data-id="${region}"]').click()
    },${tempTime})\n`
    tempTime+=3000
    script += `setTimeout(()=>{
    let queryUrl = '${target.url}'
    $.get(queryUrl)
    .then(result=>{
      ipcRenderer.send('query',{'type':'${target.type}',data:result.data.data})
    })
    },${tempTime})\n`
    tempTime+=3000
  }
  script += `setTimeout(()=>{
    location.reload()
  },${intervalSeconds})\n`
  mainWindow.webContents.on('dom-ready', function() {
    console.log(script)
    mainWindow.webContents.executeJavaScript(script)
  })
}

ipcMain.on('query', (event, arg) => {
  console.log(`Query ${arg.type} ${arg.data.length}筆`)
  targetStore = store.get(arg.type)
  if(targetStore){
    if(arg.data.length>0&&targetStore.length>0){
      let diffArr = getDiffArr(arg.data,targetStore)
      if(diffArr.length>0){
        console.log(diffArr)
        let message = '';
        for(let rentObj of diffArr){
          message += rentObj.address_img + ' ' + rentObj.price+ '\n'
        }
        notifier.notify({
            'title': '591有更新-'+arg.type,
            'message': message
        });
        mail(diffArr,arg.type)
      }
    }
  }
  store.set(arg.type,arg.data)   
})

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

function getDiffArr(newData,oldData){
  for(let i = 0; i<newData.length;i++){
    for(let j = 0; j<oldData.length;j++){
      if(newData[i].id===oldData[j].id){
        return newData.slice(0,i)
      }
    }
  }
  return []
}

function getUrlParameter(url,name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(url);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}